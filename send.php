<?php
		
	
	function sendSMS($access_token,$phone,$message){
		$url = 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/3987/requests?access_token='.$access_token;
		$data = array(
			'address' => $phone,
			'message' => $message
		);
		$data_string = json_encode($data);                                                                                   
		$ch = curl_init($url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch);
		return $result;
	}
	
	function sendSMStoAllAbsentOrLate(){
		

		$config = require('application/config.php');
		$conn = new mysqli($config['hostname'],$config['username'],$config['password'],$config['database']);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 
	    date_default_timezone_set('Asia/Hong_Kong');
        $date = date('Y-m-d');
        $time = date('Y-m-d h:i:s');
		
		// QUERY TO GET ALL ABSENT
		$phql = "select 
				(a.time > CONCAT(a.date,' 9:00') and a.status=1) as is_late,p.phone,su.access_token,s.name,a.status,a.time
				from student s 
				LEFT JOIN attendance a ON s.student_id = a.student_id
				INNER JOIN parent p ON s.parent_id = p.parent_id
				INNER JOIN subscribers su ON su.subscriber_number = p.phone
				where 
				(CONVERT(a.date,date) = CONVERT('{$date}',date) AND (a.status = 0 || a.time > CONCAT(a.date,' 9:00'))) OR a.student_id is null
				GROUP BY s.student_id
				";


		$result = $conn->query($phql);
			if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        echo "id: " . $row["access_token"];
		        $access_token = $row['access_token'];
		        $phone  = $row['phone'];
		        $late_message = 'Your child '.$row['name'].' is late today '.date('M d, Y').' he/she tap at '.$row['time'];
		        $absent_message = 'Your child '.$row['name'].' is absent today '.date('M d, Y');

		        if($row['is_late'] == '1'){
		        	$message = $late_message;	
		        }else{
		        	$message = $absent_message;
		        }


		        $sendingResults = sendSMS($access_token,$phone,$message);
		        var_dump($sendingResults);
		    }

		} else {
		    echo "No Absent/Lates";
		}
		$conn->close();
	}

	function sendSMStoLate($student_id){
		$config = require('application/config.php');
		$conn = new mysqli($config['hostname'],$config['username'],$config['password'],$config['database']);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 
	    date_default_timezone_set('Asia/Hong_Kong');
        $date = date('Y-m-d');
        $time = date('Y-m-d h:i:s');
		
		// QUERY TO GET ALL ABSENT
		$phql = "select 
				(a.time > CONCAT(a.date,' 9:00') and a.status=1) as is_late,p.phone,su.access_token,s.name,a.status,a.time
				from student s 
				LEFT JOIN attendance a ON s.student_id = a.student_id
				INNER JOIN parent p ON s.parent_id = p.parent_id
				INNER JOIN subscribers su ON su.subscriber_number = p.phone
				where 
				(CONVERT(a.date,date) = CONVERT('{$date}',date) AND (a.status = 0 || a.time > CONCAT(a.date,' 9:00'))) AND s.student_id = {$student_id}
				";


		$result = $conn->query($phql);
			if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        echo "id: " . $row["access_token"];
		        $access_token = $row['access_token'];
		        $phone  = $row['phone'];
		        $late_message = 'Your child '.$row['name'].' is late today '.date('M d, Y').' he/she tap at '.$row['time'];
		        $absent_message = 'Your child '.$row['name'].' is absent today '.date('M d, Y');

		        $message = "Good Morning Mam/ Sr,
						    This is from St. Odilard School, Your child ".$row['name']." is late today".date('M d, Y')." he/she tap at ".$row['time']."
						     Thanks and godbless";


		        $sendingResults = sendSMS($access_token,$phone,$message);
		        var_dump($sendingResults);
		    }

		} else {
		    echo "No Absent/Lates";
		}
		$conn->close();
	}

	if(isset($_GET['student_id'])){
		sendSMStoLate($_GET['student_id']);
	}else{
		sendSMStoAllAbsentOrLate();	
	}
	
?>