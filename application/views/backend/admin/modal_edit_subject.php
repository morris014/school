<?php 
$edit_data		=	$this->db->get_where('subject' , array('subject_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_subject');?>
            	</div>
            </div>
			<div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/subject/do_update/'.$row['subject_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('class');?></label>
                    <div class="col-sm-5 controls">
                        <select name="class_id" class="form-control">
                            <?php 
                            $classes = $this->db->get('class')->result_array();
                            foreach($classes as $row2):
                            ?>
                                <option value="<?php echo $row2['class_id'];?>"
                                    <?php if($row['class_id'] == $row2['class_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('teacher');?></label>
                    <div class="col-sm-5 controls">
                        <select name="teacher_id" class="form-control">
                            <option value=""></option>
                            <?php 
                            $teachers = $this->db->get('teacher')->result_array();
                            foreach($teachers as $row2):
                            ?>
                                <option value="<?php echo $row2['teacher_id'];?>"
                                    <?php if($row['teacher_id'] == $row2['teacher_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <?php
                    $this->db->where('subject_id' , $row['subject_id']);
                    $this->db->where('class_id' , $row['class_id']);
                    $routines   =   $this->db->get('class_routine')->result_array();
                    $time_start = 0;
                    $time_end = 0;

                    if(sizeof($routines) > 0){
                        $time_start  = $routines[0]['time_start'];
                        $time_start_a = $time_start > 12;
                        $time_start = ($time_start > 12) ? $time_start - 12 : $time_start;
                        $time_end  = $routines[0]['time_end'];
                        $time_end = ($time_end > 12) ? $time_end - 12 : $time_end;
                        $time_end_a = $time_end > 12;

                    }
                ?>  
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('starting_time');?></label>
                    <div class="col-sm-5">
                        <select name="time_start" class="form-control" style="width:100%;">
                            <?php for($i = 0; $i <= 12 ; $i++):?>
                                <option <?php if($time_start == $i) echo 'selected';?> value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php endfor;?>
                        </select>
                        <select name="starting_ampm" class="form-control" style="width:100%">
                            <option <?php if(!$time_start_a) echo 'selected'; ?> value="1">am</option>
                            <option <?php if($time_start_a) echo 'selected'; ?> value="2">pm</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('ending_time');?></label>
                    <div class="col-sm-5">
                        <select name="time_end" class="form-control" style="width:100%;">
                            <?php for($i = 0; $i <= 12 ; $i++):?>
                                <option <?php if($time_end == $i) echo 'selected';?> value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php endfor;?>
                        </select>
                        <select name="ending_ampm" class="form-control" style="width:100%">
                            <option <?php if(!$time_end_a) echo 'selected'; ?> value="1">am</option>
                            <option <?php if($time_end_a) echo 'selected'; ?> value="2">pm</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_subject');?></button>
                    </div>
                 </div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>



