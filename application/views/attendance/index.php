<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $system_name    =   $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
    $system_title   =   $this->db->get_where('settings' , array('type'=>'system_title'))->row()->description;
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
    
    <title><?php echo get_phrase('Attendance');?> | <?php echo $system_title;?></title>
    

    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="assets/images/favicon.png">
    
</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '<?php echo base_url();?>';
</script>

<div class="login-container">
    
    <div class="login-header login-caret">
        
        <div class="login-content" style="width:100%;">
            
            <a href="<?php echo base_url();?>" class="logo">
                <img src="uploads/logo.png" height="60" alt="" />
            </a>
            
            <p class="description">
                <h2 style="color:#cacaca; font-weight:100;">
                    <?php echo $system_name;?>
              </h2>
           </p>
            
            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
                <h3>43%</h3>
                <span>logging in...</span>
            </div>
        </div>
        
    </div>
    
    <div class="login-progressbar">
        <div></div>
    </div>
    

    <div class="login-form">
        <div id="frmDetails" class="row" hidden>
           <div class="col-md-4 col-md-offset-4">
                <div class="col-md-6">
                    <img src="https://lh4.googleusercontent.com/-r4oK9mG9Ilk/AAAAAAAAAAI/AAAAAAAABek/uvyKKVDO434/photo.jpg" style="height:250px;width:250px" class="img-rounded img-responsive">
                </div>
                <div class="col-md-6">
                    <label>ID</label>
                    <input id="rfid" type="text" class="form-control" placeholder="">
                    <label>Name</label>
                    <input id="name" type="text" class="form-control" placeholder="">
                    <label>Section</label>
                    <input id="section" type="text" class="form-control" placeholder="">
                </div>

                    
            </div>
    </div>    
        <div class="login-content">
            
            
            <form method="post" role="form" id="form_login">
                <div class="form-group">
                <label>Please Tap Your ID</label>
                </div>
            </form>
        </div>
        
    </div>
    
</div>
        

    <!-- Bottom Scripts -->
    <script src="assets/js/gsap/main-gsap.js"></script>
    <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/neon-api.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/neon-login.js"></script>
    <script src="assets/js/neon-custom.js"></script>
    <script src="assets/js/neon-demo.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){   
        function getStudent(rfid){
            return $.ajax({
                url: 'index.php?attendance/student/' + rfid,
                method : 'get',
                dataType : 'json'
            });
        }
        function sendSMS(studentId){
            return $.ajax({
                url: 'send.php?student_id=' + studentId,
                method : 'get',
                dataType : 'json'
            });   
        }
        var chars = [];
        var pressed = false;
        var timer ;
        $(window).keypress(function(e){
            function initInfo(rfid,name,section){
                $('#rfid').val(rfid);
                $('#name').val(name);
                $('#section').val(section);
            }
            function clearInfo(){
                $('#rfid').val('');
                $('#name').val('');
                $('#section').val('');   
            }
             if (e.which >= 48 && e.which <= 57) {  //PARA SA NUMBERS LANG
                chars.push(String.fromCharCode(e.which)); // CONVERT UNG NUMBER TO CHAR  

                if(pressed == false){

                    setTimeout(function(){
                        // check we have a long length e.g. it is a barcode
                            if (chars.length >= 8 || chars.length == 3  ) {
                                clearInfo();
                                clearTimeout(timer);
                                // join the chars array to make a string of the barcode scanned
                                var barcode = chars.join("");
                                // debug barcode to console (e.g. for use in Firebug)
                                console.log("Barcode Scanned: " + barcode);
                                // assign value to some input (or do whatever you want)
                                $('#rfid').val(barcode);

                                getStudent(barcode).done(function(result){
                                    if(result.found == 1){
                                        initInfo(result.result.rfid,result.result.name,result.result.section);
                                        $('#frmDetails').show();

                                        sendSMS(result.student_id).done(function(res){
                                            console.log('RES:',res);
                                            console.log('SMS SENT');
                                        });

                                        timer = setTimeout(function(){
                                            $('#frmDetails').hide();
                                        },3000)
                                    }
                                });


                                
                            }    
                        chars = [];
                        pressed = false;
                    },500);
                }
            }   
            pressed = true;
        });

        $('#rfid').keypress(function(e){
            if(e.which == 13){
                console.log('Prevent form submit');
                e.preventDefault();
            }
        }); 
    });
    </script>
</body>
</html>