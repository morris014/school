<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Attendance extends CI_Controller
{
    
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->library('session');
		
       /*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		
    }
    
    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {

        $page_data['page_name']  = 'attendance';
        $page_data['page_title'] = get_phrase('attendance');
        $this->load->view('attendance/index', $page_data);
    }
    
    public function student($rfid = ''){
        $this->db->select('s.student_id,s.name,s.rfid,s.section_id,se.name as section');
        $this->db->from('student s');
        $this->db->join('section se','se.section_id = s.section_id','left');
        $this->db->where('rfid',$rfid);
        $query = $this->db->get();
        $result = $query->result();

        if(sizeof($result) > 0){
            date_default_timezone_set('Asia/Hong_Kong');
            $date = date('Y-m-d');
            $time = date('Y-m-d H:i:s');
            $student_id = $result[0]->student_id;
            $status = 1;
            if(new DateTime($time) > new DateTime($date.' 9:00')){
                $status = 3;
            }
            $phql = "INSERT INTO attendance(status,student_id,`date`,time) VALUES ({$status},'{$student_id}','{$date}','{$time}')
            ON DUPLICATE KEY UPDATE time='{$time}',status={$status};";
            $results = $this->db->query($phql);


            echo json_encode(array('result'=> $result[0],'found' =>1,'results'=>$results,'student_id'=>$student_id));    
        }else{
            echo json_encode(array('found' => 0));    
        }
    }

    public function sendSmsToLate($student_id){
        
        $config = require(dirname(__FILE__).'/../config.php');
        $conn = new mysqli($config['hostname'],$config['username'],$config['password'],$config['database']);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        date_default_timezone_set('Asia/Hong_Kong');
        $date = date('Y-m-d');
        $time = date('Y-m-d h:i:s');
        
        // QUERY TO GET ALL ABSENT
        $phql = "select 
                (a.time > CONCAT(a.date,' 9:00') and a.status=1) as is_late,p.phone,su.access_token,s.name,a.status,a.time
                from student s 
                LEFT JOIN attendance a ON s.student_id = a.student_id
                INNER JOIN parent p ON s.parent_id = p.parent_id
                INNER JOIN subscribers su ON su.subscriber_number = p.phone
                where 
                ((CONVERT(a.date,date) = CONVERT('{$date}',date) AND (a.status = 0 || a.time > CONCAT(a.date,' 9:00'))) OR a.student_id is null) AND s.student_id = {$student_id}
                GROUP BY s.student_id
                ";


        $result = $conn->query($phql);
            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                echo "id: " . $row["access_token"];
                $access_token = $row['access_token'];
                $phone  = $row['phone'];
                $late_message = 'Your child '.$row['name'].' is late today '.date('M d, Y').' he/she tap at '.$row['time'];
                $absent_message = 'Your child '.$row['name'].' is absent today '.date('M d, Y');

                if($row['is_late'] == '1'){
                    $message = $late_message;   
                }else{
                    $message = $absent_message;
                }


                $sendingResults = sendSMS($access_token,$phone,$message);
            }

        } else {
            echo "No Absent/Lates";
        }
        $conn->close();
        
        
    }
    
}
